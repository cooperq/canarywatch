{% extends "base.tpl" %}
{% block title %}Canary Watch - About{% endblock %}

{% block content %}
  <h1>About Canary Watch</h1>
  <p>"Warrant canary" is a colloquial term for a regularly published statement that a service provider has not received legal process that it would be prohibited from saying it had received, such as a <a href="https://www.eff.org/issues/national-security-letters">national security letter</a>. Canarywatch tracks and documents these statements. This site lists warrant canaries we know about, tracks changes or disappearances of canaries, and allows <a href="/submission.html">submissions of canaries</a> not listed on the site.</p>
    <p><a href="https://www.twitter.com/warrantcanary" target=_blank>Follow us on Twitter</a> for updates and notifications about canaries on this site.</p>

<p>Canarywatch is a coalition of organizations including the <a href="https://www.eff.org" target=_blank>Electronic Frontier Foundation</a>, <a href="http://cyber.law.harvard.edu/" target=_blank>the Berkman Center for Internet and Society</a>, <a href="http://www.law.nyu.edu/academics/clinics/semester/technologylawandpolicy" target=_blank>NYU's Technology Law &amp; Policy Clinic</a>, <a href="https://freedom.press" target=_blank>Freedom of the Press Foundation</a> and the <a href="https://calyx.net/" target=_blank>Calyx Institute</a>. The Calyx Institute runs and hosts canarywatch.org.</p>
<p> For general questions please email <a href='mailto:info@canarywatch.org'>info@canarywatch.org</a></p>

<a name="anatomy"></a><h2>Anatomy of a Warrant Canary</h2>
<p>Warrant canaries come in many forms. Service providers can adapt the design of warrant canaries to reflect the many different reasons why the providers want to speak about law-enforcement and national-security surveillance requests for customer information and communications. </p>
<p>While there is no one way to "warrant canary," every provider that wishes to publish a canary has to make choices about the following features:</p>
<p><b>Where is it published?</b></p>
<p>A warrant canary can be either a standalone statement or part of a more comprehensive transparency report. Standalone statements generally appear on a website, either as a separate page or as a simple statement published on the bottom of a website that says something like "Provider X has received 0 national security requests."</p>
<p><b>How often is it published?</b></p>
<p>Providers have to decide at what rate to publish warrant canaries. For example, some companies publish annual transparency reports with canaries. Some publish canaries bi-annually. Some providers simply leave a warrant-canary statement up until, ostensibly, it is no longer valid. </p>
<p>Apart from the frequency at which a canary is posted, a provider must consider whether or not to have a gap between the time period covered by a canary and the date the canary is posted—the so-called "latency period" of a canary). Some providers may issue a warrant canary that current through the posting date, while others may reference a particular, usually recent, time period. For example, a provider could choose to publish a report about the January&mdash;June period in August, meaning any canary contained in the report would have a two-month latency period. </p>
<p>A longer latency period, as well as reports that cover a longer period of time, give a provider time to ensure that they have done the due diligence required to put together an accurate transparency report. Additionally, a longer latency period could give the provider crucial time to challenge a gag order (although it is important to note that no provider has yet successfully challenged a gag order, and legal challenges can take years). Alternatively, shorter latencies might increase the possibility that a canary will change in a way that makes it appear a company has received legal process even if it hasn't. Of course, a shorter latency period means it becomes clear sooner to the public that a canary has "died."</p>
<p><b>What Kind of Request and/or Gag Order Is It Talking About?</b></p>
<p>There are many ways law enforcement can issue gag orders, including national-security letters, Foreign Intelligence Surveillance Court orders, court orders under the Electronic Communications Privacy Act, and others. Some providers issue warrant canaries for all national-security process, or for all law-enforcement gag orders. Others list the different legal authorities separately.</p>
<p>Legal processes that may be indicated by a warrant canary include:</p>
<ul><li>any one of <a href="https://www.eff.org/issues/national-security-letters/faq#13">several types</a> of <a href="https://www.eff.org/issues/national-security-letters">national security letters</a>;</li>
<li>orders from the Foreign Intelligence Surveillance Court, including but not limited to
<ul><li>Section 215 orders used for the bulk call-records program</li>
<li>Section 702 directives used for the NSA's PRISM program</li>
</ul></li>
<li>an ordinary subpoena or search warrant accompanied by a gag order pursuant to the Electronic Communication Privacy Act. </li>
</ul><p><b>How Do We Know It Is Real?</b></p>
<p>How a warrant canary is made public can affect the level of trust users have that it is authentic. Unless a system is in place to assure users about a canary's authenticity, users may question whether a warrant canary came from a provider or from someone pretending to be that provider. Some providers use encryption or other methods to verify the warrant canary as authentic, or to help expose a fake one. For example, <a href="https://blog.spideroak.com/20140814060007-status-reports-transparency-overall-safety">Spideroak's</a> warrant canary is "specific plain text signed with multiple GPG keys." (Of course, it's important to note that even if a canary is signed by a key, that doesn't mean that the provider wasn't forced to sign it.) Other providers simply publish their canary on a website run by the provider.</p>

<img src='/static/coalition-logos.png' width=600 alt='Coalition Logos'>
{% endblock %}
