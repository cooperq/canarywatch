{% extends "base.tpl" %}
{% block title %}Canary Watch{% endblock %}

{% block content %}
  <div id="intro-text">
    <p>"Warrant canary" is a colloquial term for a regularly published statement that a service provider has not received legal process that it would be prohibited from saying it had received, such as a <a href="https://www.eff.org/issues/national-security-letters">national security letter</a>. Canarywatch tracks and documents these statements.</p>
  </div>
  {% for canary in canaries %}
    <div class='canaryContainer'>
      <div class='canaryLogo'>
        <span class='helper'></span>
        <a href="/{{canary.slug}}/">
          <img src="/{{canary.slug}}/logo.png" alt="Logo for {{canary.name}}">
        </a>
      </div>
      <div class='canaryLink'>
        {% if canary.get_state() != 'BAD' %}
           <img src="/static/canary-logo.png">
        {% endif %}
      </div>
      <div class='clear'></div>
      <div class='canaryDetail'>
        <ul>
          <li>Organization: {{canary.config["name"]}}</li>
          <li>Last Checked: {{canary.last_update.strftime('%Y-%m-%d')}}</li>
          <li><a href="/{{canary.slug}}/">View Details &raquo;</a></li>
        <ul>
      </div>
    </div>
  {% endfor %}
{% endblock %}
