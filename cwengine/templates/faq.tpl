{% extends "base.tpl" %}
{% block title %}Canary Watch - FAQ{% endblock %}

{% block content %}
  <h1>Frequently Asked Questions</h1>
  <h2><b>What is a warrant canary?</b></h2>

<p>A warrant canary is a colloquial term for a regularly published statement that a service provider has <b>not</b> received legal process (like a national security letter) that it would be prohibited from disclosing to the public. Once a service provider does receive legal process, the speech prohibition goes into place, and the provider no longer makes the statement about the number of such process received. </p>

<p>Warrant canaries are often provided in conjunction with a transparency report, listing the processes the service provider can publicly say it received over the course of a particular time period. The term "warrant canary" is a reference to the <a href="http://en.wikipedia.org/wiki/Animal_sentinel">canaries</a> used to provide warnings in coalmines, which would become sick before miners from carbon monoxide poisoning, warning of the otherwise-invisible danger. </p>

<h2><b>How do warrant canaries work in theory?</b></h2>

<p>When a warrant canary "dies" (disappears, is not updated, etc.), provider no longer makes the statement about the number of such process received.</p>

<p>For example, an ISP might issue a semi-annual transparency report, stating that it had not received any national security letters in a particular six-month period. NSLs <a href="http://www.sfcenter.org/events/2014-bold-awards-and-reception">come with a gag</a>, which purports to prevent the recipient from saying it has received one. (While a federal court has ruled that <a href="https://www.eff.org/deeplinks/2013/03/depth-judge-illstons-remarkable-order-striking-down-nsl-statute">the NSL gag is unconstitutional</a>, that order is currently stayed pending the resolution of the government's appeal). When the ISP issues a subsequent transparency report without that statement, the reader may infer from the silence that the ISP has now received an NSL.</p>

<h2><b>How do warrant canaries work in practice?</b></h2>

<p>Unfortunately (though unsurprisingly) warrant canaries come in a variety of forms. Some are more detailed, such as the canary at Riseup.net: </p>

<blockquote>"Riseup has not received any National Security Letters or FISA court orders, and we have not been subject to any gag order by a FISA court.")</blockquote>

<p>Some are less detailed, such as the canary on Pinterest:</p>

<blockquote>National security: 0</blockquote>

<p>The variety of different forms means that each canary must be examined carefully. This <a href='/about.html#anatomy'>"anatomy of a warrant canary"</a> can help with that. It explains what specific legal processes a company might want to include in a canary, and what those processes are. </p>

<p>Unfortunately, the disappearance of or changes to a warrant canary may not always mean that the service provider received secret legal process. It may have simply decided to change the format of its transparency report, or forgotten to issue an update by the deadline. </p>

<h2><b>Why would an ISP want to publish a warrant canary?</b></h2>

<p>"<i>Sunlight is said to be the best of disinfectants</i>." &mdash; <a href="https://en.wikipedia.org/wiki/Louis_Brandeis">Justice Louis D. Brandeis</a>.</p>

<p>Publishing a warrant canary is one way for ISPs to indicate that they support greater transparency around government surveillance, and to allow them to provide honest and complete transparency reports up until the time that they are gagged. We are in a time of unprecedented public debate over the government's powers to secretly obtain information about people. The revelations about the NSA's massive bulk surveillance programs have raised serious questions about whether these powers are necessary, legal and constitutional. Secret surveillance violates not only the privacy interests of the account holder, but the speech interests of ISPs who wish to participate in these public debates. </p>

<h2><b>Why should we care about publicizing secret legal processes like national security letters?</b></h2>

<p>As part of the reauthorization of the Patriot Act in 2006, Congress directed the DOJ Inspector General (IG) to investigate and report on the FBI's use of NSLs. In four reports issued in <a href="http://www.usdoj.gov/oig/special/s0703b/final.pdf">2007</a>, <a href="http://www.usdoj.gov/oig/special/s0703b/final.pdf">2008</a>, <a href="http://www.justice.gov/oig/special/s1001r.pdf">2010</a>, and <a href="http://www.justice.gov/oig/reports/2014/s1408.pdf">2014</a> the IG documented the agency's systematic and extensive misuse of NSLs and various attempts at reform.</p>

<p>The reports showed that between 2003 and 2006, the FBI's intelligence violations included improperly authorized NSLs, factual misstatements in the NSLs, improper requests under NSL statutes, and unauthorized information collection through NSLs. The FBI's improper practices included requests for information based on First Amendment protected activity. Although the 2014 report finds that the FBI has improved NSL practices, it makes new recommendations, and  notes significant issues with how the FBI interprets the scope of its authority under NSL statutes, as well as ongoing problems with the FBI's use of <a href="https://www.eff.org/deeplinks/2010/01/fbi-replaced-legal-process-post-it-notes-obtain-ph">exigent letters</a>.</p>

<p>In December 2013, the President's Review Group on Intelligence and Communications Technologies <a href="http://www.whitehouse.gov/sites/default/files/docs/2013-12-12_rg_final_report.pdf">recommended</a> public reporting&mdash;both by the government and NSL recipients&mdash;of the number of requests made, the type of information produced, and the number of individuals whose records have been requested.</p>

<p>As discussed below, NSLs are just one type of gagged legal process. Similar <a href="http://www.washingtonpost.com/world/national-security/nsa-broke-privacy-rules-thousands-of-times-per-year-audit-finds/2013/08/15/3310e554-05ca-11e3-a07f-49ddc7417125_story.html">problems persist</a> in other forms of secret process.</p>

<h2><b>What are some of the gagged legal processes that an ISP might receive?</b></h2>

<p>An ISP may be gagged from stating it has received:</p>

<ul>
    <li>any one of several types of national security letters;</li>
      <li>orders from the Foreign Intelligence Surveillance Court (like the Section 215 orders used for the bulk call records program or the Section 702 orders used for the NSA's PRISM program); or</li>
        <li>an ordinary subpoena or search warrant accompanied by a gag order pursuant to the Electronic Communication Privacy Act. </li>
      </ul>

<p>      The government has issued hundreds of thousands of these gagged legal requests, but very few have ever seen the light of day.</p>

<h2>      <b>What does the government say is permissible for recipients of gagged legal process?</b></h2>

<p>      Being subject to a gag order means that companies can't make simple statements like "We received three NSLs last year." Instead, the government only <a href="http://www.justice.gov/iso/opa/resources/366201412716018407143.pdf">allows</a> ISPs to report receipt of gagged legal process in ranges of 1000, starting at 0, for six-month periods. So if an ISP received 654 NSLs, it could report 0-999. If the companies choose to report FISC requests and NSL requests combined, they can use ranges of 250, again starting at 0. For example, Apple reported receiving 0-249 national security requests in the first half of 2013 and AT&amp;T reported 0-999 content FISC orders, 0-999 non-content FISC orders and 2000-2999 NSLs for the same period.</p>

<p>      While the government-approved ranges all start at zero, publication of a range might indicate that the ISP has received at least one, as otherwise the ISP would have no obligation to follow the government's formula.</p>

<p>      In contrast to the government-approved ranges, warrant canaries can be much more specific, making it easier to determine what sort of legal process an ISP has been served with.</p>

<h2>      <b>Is it legal to publish a warrant canary?</b></h2>

<p>      There is no law that prohibits a service provider from publishing an honest and complete transparency report that includes all the legal processes that it has <b>not</b> received. The gag order only attaches after the ISP has been served with the gagged legal process. Nor is publishing a warrant canary an obstruction of justice, since this intent is not to harm the judicial process, but rather to engage in a public conversation about the extent of government investigatory powers.</p>

<h2>      <b>What's the legal theory behind warrant canaries?</b></h2>

<p>      The legal theory behind warrant canaries is based on the concept of <i>compelled speech</i>. Compelled speech is where a party is forced by the government to make expressive statements. The First Amendment protects against compelled speech in most circumstances. For example, a court held that the New Hampshire state government could not require its citizens to have "Live Free or Die" on their license plates. While the government may be able to compel silence about legal processes through a gag order, it's much more difficult to argue that it can compel an ISP to lie by falsely stating that it has not received legal process when in fact it has. </p>

<h2>      <b>How have courts dealt with compelled speech?</b></h2>

<p>      Courts have rarely upheld compelled speech. In a few instances, courts have allowed the government to compel speech in the commercial context, where the government shows that the compelled statements convey important truthful information to consumers. For example, warnings on cigarette packs are a form of compelled commercial speech that have sometimes been upheld, and sometimes struck down, depending on whether the government shows there is a rational basis for the warning. Existing precedents, however, are unlikely to extend to this situation. </p>

<p>    <b>We're not aware of any case where a court has upheld compelled false speech&mdash;</b>and the cases on compelled speech have tended to rely on truth as a minimum requirement. For example, Planned Parenthood challenged a requirement that physicians tell patients seeking abortions of an increased risk of suicidal ideation. The court found that Planned Parenthood did not meet its burden of showing that the disclosure was untruthful, misleading, or not relevant to the patent's decision to have an abortion.</p>

<p>    That's why the theory provides a strong legal underpinning for the use of warrant canaries&mdash; having a pre-existing statement regarding legal processes means that any change to that statement required by the government would be compelled speech.</p>

<h2>    <b>Are there any cases upholding warrant canaries?</b></h2>

<p>    Not yet, because as far as we know, no court has ever been called on to rule on one. However, that may change soon. In October 2014, Twitter filed a lawsuit against the federal government after the FBI prevented Twitter from publishing an April 2014 transparency report. Google sued in the Foreign Intelligence Surveillance Court to be able to publish aggregate numbers of national security requests in a transparency report, and was joined by Yahoo!, Microsoft, LinkedIn, and Facebook. Those companies reached a compromise on what they could publish, outlined in a <a href="https://www.eff.org/document/deputy-attorney-general-transparency-reporting-letter" target=_blank>January 2014 letter</a> from the Deputy Attorney General (the DAG letter). Although Twitter wasn't a party to this action, "the DOJ and FBI told Twitter that the DAG Letter sets forth the limits of permissible transparency-related speech for Twitter." The DAG letter provides two options for reporting, but as the complaint specifically notes: "Under either option, since the permitted ranges begin with zero, service providers who have never received an NSL or FISA order apparently are prohibited from reporting that fact." Twitter notes in the complaint that, "Notwithstanding the fact that the DAG Letter purportedly prohibits a provider from disclosing that it has received 'zero' NSLs or FISA orders, or 'zero' of a certain kind of FISA order, subsequent to January 27, 2014, certain communications providers have publicly disclosed either that they have never received any FISA orders or NSLs, or any of a certain kind of FISA order. This is referring to the use of what are colloquially called warrant canaries.</p>

<p>    Twitter's filing argues it shouldn't be bound by a settlement it wasn't a party to, and asks the court for a declaratory judgment (a statement from the court) saying so. It also asks the court to let it publish the draft transparency report. </p>

<p>    The outcome of Twitter's case may take years. But we believe that warrant canaries are legal, and the government should not be able to compel a lie. To borrow a phrase from <a href="http://books.google.com/books?id=o6rFno1ffQoC&amp;lpg=PA215&amp;ots=Qyg9IdLg2_&amp;dq=quote%20%22no%20one%20can%20guar%C2%ADan%C2%ADtee%20suc%C2%ADcess%20in%20war%2C%20but%20only%20deserve%20it%22&amp;pg=PA215#v=onepage&amp;q=quote%20%22no%20one%20can%20guar%C2%ADan%C2%ADtee%20suc%C2%ADcess%20in%20war,%20but%20only%20deserve%20it%22&amp;f=false">Winston Churchill</a>, no one can guarantee success in litigation, but only deserve it.</p>

<h2>    <b>What should an ISP do if the warrant canary is triggered?</b></h2>

<p>    If an ISP with a warrant canary receives gagged legal process, it should obtain legal counsel and go to a court for a determination that it cannot be required to publish false information. While some ISPs may be tempted to engage in civil disobedience, we believe that it is better to present the issue to a court <i>before removing the canary</i>, to help establish a precedent. If you run an ISP with a warrant canary and receive gagged legal process, you can contact <a href="mailto:info@canarywatch.org">info@canarywatch.org</a> if you would like help finding counsel. </p>

<h2>    <b>How often should an ISP publish the warrant canary?</b></h2>

<p>    Various ISPs have published canaries on a wide range of schedules. To allow time to file a case and for the court to rule on the important legal questions, we suggest at least few months between the transparency report and the time period covered.</p>

<h2>    <b>Who is involved in Canarywatch?</b></h2>

<p>    Canarywatch is a coalition of organizations including the <a href="https://www.eff.org" target=_blank>Electronic Frontier Foundation</a>, <a href="http://cyber.law.harvard.edu/" target=_blank>the Berkman Center for Internet and Society</a>, <a href="http://www.law.nyu.edu/academics/clinics/semester/technologylawandpolicy" target=_blank>NYU's Technology Law &amp; Policy Clinic</a>, <a href="https://freedom.press" target=_blank>Freedom of the Press Foundation</a> and the <a href="https://calyx.net/" target=_blank>Calyx Institute</a>. The Calyx Institute runs and hosts canarywatch.org.</p>
  
{% endblock %}
