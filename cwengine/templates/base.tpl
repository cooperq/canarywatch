<html>
  <head>
    <title> {% block title %}{% endblock %} </title>
    <link rel='stylesheet' type='text/css' href='/static/canarywatch.css'>
    <meta property="og:image" content="/static/CANARY-WATCH-GRAPHIC.png"/>
    <meta property="og:title" content="Canarywatch"/>
    <meta property="og:description" content="Canarywatch lists warrant canaries on the web, tracks changes or disappearances of canaries, and allows submissions of canaries not listed on the site."/>
  </head>
  <body>
    <div id='outer-container'>
      <div id='top-bar'></div>
      <div id='header-bg'></div> <div id='inner-container'>
        <div id='menu'>
          <ul>
            <li><a href='/'>Canary List</a></li>
            <li><a href='/about.html'>About</a></li>
            <li><a href='/faq.html'>FAQ</a></li>
            <li><a href='https://twitter.com/warrantcanary'><button id='tw-link' title='A link to the canary watch twitter account'></button></a></li>
          </ul>
          <div class="clear"></div>
        </div>
        <div id='header-container'>
          <a href='/'>
          <div id='title'>
            <div id='title-left'>
              Canary
            </div>
            <div id='logo'>
              <img src='/static/canary-logo.png'>
            </div>
            <div id='title-right'>
              Watch 
            </div>
          </div>
          </a>
          <div class="clear"></div>
        </div>
        <div id='content-container'> 
          <div id='shutdown'>
            <span class='strong'>Attention:</span> Canary Watch is no longer being maintained, and the links listed have not been updated past the last checked date. Please <a href="https://eff.org/deeplinks/2016/05/canary-watch-one-year-later">read our blog post</a> for more info.
          </div>
        {% block content %}{% endblock %}
        </div>
      </div>
      <div id='bottom-bar'>
        <div id='footer'>
          <div id='contact'>
            <a href="mailto:info@canarywatch.org">Contact</a>
          </div>
          <div id='cc-license'>
            <a href="https://creativecommons.org/licenses/by/3.0/us/" target=_blank>
              <img src="/static/cc-by-logo.png" />
            </a>
          </div>
          <div id='rss-icon'>
            <a href="https://twitrss.me/twitter_user_to_rss/?user=warrantcanary" target=_blank>
              <img src="/static/rss-icon.png" />
            </a>
          </div>
        </div> <!-- end footer -->
      </div>
    </div>
  </body>
</html>

