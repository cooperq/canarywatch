{% extends "base.tpl" %}
{% block title %}Canary Watch - {{canary.config['name']}}{% endblock %}

{% block content %}
  <div class='canary-detail-header'>
  <br>
    <img src="/{{canary.slug}}/logo.png" alt="Logo for {{canary.name}}">
    <h1>{{canary.config['name']}}</h1>
  </div>
  <ul>
    <li><a href="{{canary.config['canary_url']}}" target=_blank>Current Canary</a></li>
    <li>Last Checked: {{canary.last_update.strftime('%Y-%m-%d')}}</li>
  </ul>

  {% for crawl in crawl_info %}
    <div class='canary-crawl'>
      <h2>{{crawl['date']}}</h2>
      <p>{{crawl['message']}}</p>
      {% if crawl['diff'] %}
        <a href={{crawl['diff']}}>View Changes</a>
      {% endif %}
      {% if crawl['archive'] %}
        <a href={{crawl['archive']}}>Snapshot</a>
      {% endif %}
      {% if crawl['diff'] %}
        <a href={{crawl['more']}}>More information</a>
      {% endif %}
    </div>
  {% endfor %}
{% endblock %}
