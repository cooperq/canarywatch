from canary import CanaryException

def filter(raw_text, canary):
    if canary.first_run:
        return raw_text
    raise CanaryException("Time to check for a new transparency report from " + canary.config['name'])
