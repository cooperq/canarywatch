""" This file is part of Canary Wach.

    Canary Wach is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Canary Wach is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.  
    You should have received a copy of the GNU General Public License
    along with Canary Wach.  If not, see <http://www.gnu.org/licenses/>.
"""
import re
from canary import CanaryException

def filter(raw_text, canary):
    date_regex = canary.config['date_regex']
    clean = re.sub(date_regex, '[DATE]', raw_text)
    return clean
