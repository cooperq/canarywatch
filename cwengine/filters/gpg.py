""" This file is part of Canary Wach.

    Canary Wach is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Canary Wach is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.  
    You should have received a copy of the GNU General Public License
    along with Canary Wach.  If not, see <http://www.gnu.org/licenses/>.
"""
import gnupg, os
from canary import CanaryException
DATA_STORE = "../datastore"

gpg = gnupg.GPG(homedir=os.path.join(DATA_STORE, '.gpg'))

def filter(raw_text, canary):
    pubkey = open(os.path.join(canary.config_path, 'pubkey.gpg')).read()
    gpg.import_keys(pubkey)
    verified = gpg.verify(raw_text)
    if not verified.valid:
        raise CanaryException("Bad GPG signature for " + canary.slug + ": " + verified.stderr)

    #gpg.decrypt should strip out the gpg signature
    decrypt = gpg.decrypt(raw_text)
    return decrypt.data
