""" This file is part of Canary Wach.
    Canary Wach is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Canary Wach is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Canary Wach.  If not, see <http://www.gnu.org/licenses/>.
"""

from datetime import datetime, date
import git
import json
import os
import requests
import sys
import time
import utils
import yaml

# Local imports
import config

class Canary(object):
    """ A class that defines a canary
    Contains config data for a given canary and
    methods to do a crawl, do a diff and record data
    about a warrant canary.
    """

    STATE_OKAY = "OKAY"
    STATE_UNKNOWN = "UNKNOWN"
    STATE_BAD = "BAD"
    CONFIG_DIR = "../config"
    DATA_STORE = "../datastore"
    CONFIG_NAME = "config.yml"

    sys.path.append(CONFIG_DIR)

    def __init__(self, config_path):
        """ Initialize a new Canary
        Takes a config path and a datastore path and
        makes a new canary object. Loads the config info
        from config.yml and creates an empty datastore directory
        if one does not already exist.

        Arguments:
            config_path - a path to the config dir for this canary

        Return:
            Returns a canary object
        """

        self.slug = config_path.split('/')[-1]
        self.config_path = config_path
        self.logo_path = os.path.join(self.config_path, 'logo.png')
        self.ds_path = os.path.join(self.DATA_STORE, self.slug)
        self.metadata_file = os.path.join(self.ds_path, 'metadata.json')
        self.log_file = os.path.join(self.ds_path, 'log.txt')
        self.first_run = False
        self.raw_text = ""
        self.clean_text = ""
        self.encoding = ""
        self.archive_url = ""

        try:
            conf_file = open(os.path.join(self.config_path, self.CONFIG_NAME))
            self.config = yaml.load(conf_file)
        except IOError:
            err = "ERROR: Unable to find config file for " + self.slug
            utils.bail(err)

        if not os.path.exists(self.ds_path):
            self._initialize_datastore()
            self._touch_last_good_date()
            self.first_run = True
        else:
            self.ds_repo = git.Repo(self.ds_path)

        self.last_update = datetime.fromtimestamp(
                int(self.get_meta_value('last_good', 0)))

    def check_status(self):
        """ Checks status of a warrant canary
            Fetches the canary and runs the following
            steps, halting if any step fails:
            * blank out metadata
            * Fetch raw canary
            * Put canary through all filter functions
            * Check canary for diff

            returns true if canary is as expected
            raises CanaryException if there is any problem with the canary.
        """

        # Blank metadata
        self._update_meta({
            "url": self.config["canary_url"],
            "fetch_time": int(time.time()),
            "response_headers": "",
            "last_error": "",
            "archive_url": "",
            })

        # Fetch Raw Canary
        try:
            self.raw_text = self._fetch_raw()
            with open(os.path.join(self.ds_path, 'raw'), 'w') as raw:
                raw.write(self.raw_text.encode(self.encoding))
            self._create_archive()
        except requests.exceptions.RequestException as err:
            self._handle_network_error("Network Error: " + str(err.message))
            return False

        # Run canary through filters
        filtered_text = self.raw_text

        for txt_filter in self.config['filters']:

            if os.path.isfile(os.path.join('.', 'filters', txt_filter + '.py')):
                mod_name = '.'.join(['filters', txt_filter])
            elif os.path.isfile(os.path.join(self.CONFIG_DIR, self.slug, txt_filter + '.py')):
                mod_name = '.'.join([self.slug, txt_filter])
            else:
                raise CanaryException('Filter not found: ' + txt_filter)

            module = getattr(__import__(mod_name), txt_filter)

            try:
                filtered_text = module.filter(filtered_text, self)
            except StandardError as err:
                self._handle_sanitizer_error(str(err))
                return False

        self.clean_text = filtered_text

        with open(os.path.join(self.ds_path, 'clean'), 'w') as clean:
            clean.write(self.clean_text.encode(self.encoding))

        # Check diff of canaries
        git_inst = self.ds_repo.git
        diff = git_inst.diff('clean')
        if diff == '':
            self._log("Successfully checked canary - no changes")
            self._touch_last_good_date()
            self._commit_changes(git_inst)
            return True
        else:
            self._handle_git_diff(diff)
            return False


    def needs_recheck(self):
        """ Boolean - Return true if canary is not BAD
        and is past last check period """
        return (self.get_state() != self.STATE_BAD) \
            and self._last_check_past_period()


    def set_state(self, status):
        """ Set the state of the canary to one of
        Canary.[STATE_BAD, STATE_UNKNOWN, STATE_OKAY] """
        if not status in [self.STATE_OKAY, self.STATE_UNKNOWN, self.STATE_BAD]:
            utils.bail('Attempted to set invalid status')
        if status in [self.STATE_OKAY, self.STATE_BAD]:
            self._touch_last_good_date()
        with open(os.path.join(self.ds_path, 'status'), 'w') as sfile:
            sfile.write(status)

    def get_state(self):
        """ Get current canary status as string """
        with open(os.path.join(self.ds_path, 'status'), 'r') as sfile:
            return sfile.read()


    def get_meta_value(self, key, default=None):
        """ Get a value stored in a key from the metadata
        or return a default """
        meta = self._get_meta()
        if key in meta:
            return meta[key]
        else:
            return default

    """
        Private Methods
    """

    def _last_check_past_period(self):
        """ check if the canary needs to be checked again """
        # Period is expressed in days in the config file, convert to seconds
        period_sec = self.config['period'] * 24 * 60 * 60
        return int(self._get_meta()['fetch_time']) + period_sec < time.time()

    def _commit_changes(self, git_inst):
        """ git commit changes in datastore """
        git_inst.add(A=True)
        commit_message = 'Canary watch crawl on {0}'.format(time.strftime("%c"))
        git_inst.commit(m=commit_message)

    def _fetch_raw(self):
        """Gets the raw canary file from the config url
        Return:
            A string containing the raw canary text
        """

        resp = requests.get(self.config["canary_url"])
        self.encoding = resp.encoding
        resp.raise_for_status()
        resp.headers['status_code'] = resp.status_code
        self._update_meta({"response_headers": str(resp.headers)})
        return resp.text

    def _create_archive(self):
        """ Creates an Archive in perma.cc of the canary """
        api_key = config.PERMA_API_KEY
        create_url = "https://api.perma.cc/v1/archives/?api_key={0}".format(api_key)
        archive_title = "Canarywatch crawl for {0} on {1}".format(self.config["name"], str(date.today()))
        create_payload = {"url": self.config["canary_url"], "title": archive_title}

        resp = requests.post(create_url, data=json.dumps(create_payload))
        if resp.status_code >= 300:
            print "Failed to create archive!"
            return
        guid = resp.json()['guid']
        self.archive_url = "https://perma.cc/{0}?type=image".format(guid)
        self._update_meta({"archive_url": self.archive_url})

        #move to canarywatch folder
        folder_id = config.PERMA_FOLDER_ID
        move_url = "https://api.perma.cc/v1/folders/{0}/archives/{1}/?api_key={2}".format(folder_id, guid, api_key)
        requests.put(move_url)


    def _initialize_datastore(self):
        """ create the datastore for the first time """
        os.makedirs(self.ds_path)
        self.ds_repo = git.Repo.init(self.ds_path)
        meta = {"url": "", "fetch_time": 0, "response_headers": "", "last_error": "", "diff": "", "last_good": ""}
        with open(self.metadata_file, 'w') as meta_file:
            meta_file.write(json.dumps(meta))
        self.set_state(self.STATE_OKAY)

    def _handle_network_error(self, err_str):
        """ Fail gracefully from a network error """
        clean_path = os.path.join(self.ds_path, 'clean')
        raw_path = os.path.join(self.ds_path, 'raw')
        if os.path.exists(clean_path):
            os.remove(os.path.join(self.ds_path, 'clean'))
        if os.path.exists(raw_path):
            os.remove(os.path.join(self.ds_path, 'raw'))
        self._handle_generic_error(err_str)

    def _handle_sanitizer_error(self, err_str):
        """ Fail gracefully from an error in a filter """
        clean_path = os.path.join(self.ds_path, 'clean')
        if os.path.exists(clean_path):
            os.remove(clean_path)
        self._handle_generic_error(err_str)

    def _handle_git_diff(self, diff):
        """ Fail gracefully when there is an unexpected diff between two canaries """
        filepath = os.path.join(self.ds_path, '{0}.diff'.format(int(time.time())))
        with open(filepath, 'w') as diff_file:
            diff_file.write(diff.encode('utf8'))
        self._handle_generic_error('Diff detected in canary for: ' + self.slug)

    def _handle_generic_error(self, error):
        """ Write logs and clean up state when an error happens """
        self.set_state(self.STATE_UNKNOWN)
        self._update_meta({"last_error": error})
        self._log(error)
        self._commit_changes(self.ds_repo.git)

    def _log(self, message):
        """ Log a message in the datastore """
        with open(self.log_file, 'a') as log:
            if self.archive_url:
                message += "\t" + self.archive_url
            log.write("{0}: {1}\n".format(str(date.today()), message))

    def _touch_last_good_date(self):
        """ Update the last good check of the canary """
        self._update_meta({"last_good": int(time.time())})

    def _get_meta(self):
        """ Fetch metadata for canary """
        with open(self.metadata_file, 'r+') as stream:
            metadata = json.loads(stream.read())
            return metadata

    def _update_meta(self, new_meta):
        """ Update the canary metadata """
        with open(self.metadata_file, 'r+') as stream:
            metadata = json.loads(stream.read())
            metadata.update(new_meta)
            stream.seek(0)
            stream.truncate()
            stream.write(json.dumps(metadata))

    def info(self):
        """ Return an object containing canary info """
        with open(os.path.join(self.config_path, 'site_log.yml')) as log:
            crawl_info = yaml.load(log)
            for info in crawl_info:
                info['date'] = str(info['date'])


        return {
            "Name": self.config["name"],
            "URL": self.config["canary_url"],
            "Type": self.config["canary_type"],
            "Logo": "https://canarywatch.org/{0}/logo.png".format(self.slug),
            "Status": self.STATE_BAD if (self.get_state() == self.STATE_BAD) else self.STATE_OKAY,
            "Messages": crawl_info,
            "Last Check": self._get_meta()['last_good']
            }

    def __repr__(self):
        return str(self.info())

class CanaryException(StandardError):
    """ Generic canary exception class """
    pass
