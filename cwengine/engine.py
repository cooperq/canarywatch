#!/usr/bin/env python
""" This file is part of Canary Wach.

    Canary Wach is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Canary Wach is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Canary Wach.  If not, see <http://www.gnu.org/licenses/>.
"""
# Local imports
from canary import Canary
import config

import argparse
import fnmatch
from jinja2 import Environment, FileSystemLoader
import json
import os
from random import randint
import shutil
import sys
from termcolor import colored
from urlparse import urlparse
import utils
import yaml

CONFIG_DIR = Canary.CONFIG_DIR

def _main():
    """ Main program entry function.
        Checks arguments and calls appropriate functions
    """
    parser = argparse.ArgumentParser(description="A script to check warrant canaries for takedowns or diffs", prog="canarywatch")
    parser.add_argument('command', help='command to run. one of "check", "info", "set-status", or "report"')
    parser.add_argument('--force', '-f', dest='force', action='store_true', help='force check on all canaries')
    parser.add_argument('--canary', '-c', nargs=1, type=str, help='canary to act on (checks all canaries of omitted)')
    parser.add_argument('--status', '-s', nargs=1, type=str, help='status to set, either "OKAY" or "BAD", only works with the "status" command')
    parser.set_defaults(force=False)


    args = parser.parse_args()
    if args.command == 'check':
        if args.canary:
            force_check(args.canary[0])
        else:
            check_all(args.force)
    elif args.command == 'info':
        text_report()
    elif args.command == 'set-status':
        set_status(args)
    elif args.command == 'report':
        build_report()
    else:
        utils.bail("Unknown Command: {0}".format(args.command))

def check_all(force):
    """ Check all warrant canaries for changes.
    Loops through all canaries in the config dir
    """
    print "Checking all canaries..."
    none_checked = True
    for path, dirnames, files in os.walk(CONFIG_DIR):
        if 'config.yml' in files:
            canary = Canary(path)
            if force or canary.needs_recheck():
                none_checked = False
                canary.check_status()
                print "Status for {0}: {1}".format(canary.config["name"], canary.get_state())
                if canary.get_state() != canary.STATE_OKAY:
                    print canary.get_meta_value('last_error')
    if none_checked:
        print "No canaries needed checking."


def set_status(args):
    """ update status for a single canary """
    if not args.canary or not args.status:
        utils.bail("Usage: canarywatch set-status -c <canary> -s <status>")
    slug = args.canary[0]
    status = args.status[0].upper()
    if not status in ['OKAY', 'BAD']:
        utils.bail("Status must be either 'OKAY' or 'BAD'")
    canary = Canary(os.path.join(CONFIG_DIR, slug))
    canary.set_state(status)
    print "Status for {0} set to {1}".format(slug, status)

def force_check(slug):
    """ Check a single canary """
    canary = Canary(os.path.join(CONFIG_DIR, slug))
    print "Checking canary for", canary.config["name"]
    canary.check_status()
    print "Status for {0}: {1}".format(canary.config["name"], canary.get_state())
    if canary.get_state() != canary.STATE_OKAY:
        print canary.get_meta_value('last_error')

def build_report():
    """ build canarywatch website """
    outpath = './report'
    shutil.rmtree(outpath)
    os.mkdir(outpath)

    canaries = []
    for path, dirnames, files in os.walk(CONFIG_DIR):
        if 'config.yml' in files:
            canaries.append(Canary(path))

    json_canaries = [canary.info() for canary in canaries]

    sort_order = _build_sort_order(canaries)

    canaries.sort(key=lambda canary: sort_order[canary.slug])

    env = Environment(loader=FileSystemLoader('./templates'))
    index_tpl = env.get_template('index.tpl')
    with open(os.path.join(outpath, 'index.html'), 'w') as index:
        index.write(index_tpl.render(canaries=canaries))
    with open(os.path.join(outpath, 'canaries.json'), 'w') as api:
        api.write(json.dumps(json_canaries))
    faq_tpl = env.get_template('faq.tpl')
    with open(os.path.join(outpath, 'faq.html'), 'w') as faq:
        faq.write(faq_tpl.render(canaries=canaries))
    about_tpl = env.get_template('about.tpl')
    with open(os.path.join(outpath, 'about.html'), 'w') as about:
        about.write(about_tpl.render(canaries=canaries).encode('utf-8'))

    submission_tpl = env.get_template('submission.tpl')
    with open(os.path.join(outpath, 'submission.html'), 'w') as submission:
        submission.write(submission_tpl.render(canaries=canaries))

    for canary in canaries:
        canarydir = os.path.join(outpath, canary.slug)
        os.mkdir(canarydir)
        canary_tpl = env.get_template('canary-detail.tpl')
        shutil.copy(canary.logo_path, os.path.join(canarydir, 'logo.png'))

        with open(os.path.join(canary.config_path, 'site_log.yml')) as log:
            crawl_info = yaml.load(log)

        with open(os.path.join(canarydir, 'index.html'), 'w') as index:
            index.write(canary_tpl.render(canary=canary, crawl_info=reversed(crawl_info)))

        for diff in fnmatch.filter(os.listdir(canary.ds_path), '*.diff'):
            shutil.copy(os.path.join(canary.ds_path, diff), os.path.join(canarydir, diff))

    shutil.copytree('./templates/static', './report/static')
    shutil.copy('./templates/static/favicon.ico', './report/favicon.ico')
    print 'Report generated at ' + outpath

def _print_status(status):
    """ pretty format for printing status """
    if status:
        sys.stdout.write(colored("Unchanged.\n", "green"))
    else:
        sys.stdout.write(colored("Unknown.\n", "yellow"))

def _build_sort_order(canaries):
    """ build a dictionary where the key is a canary slug and the
    value is a numeric sort order """

    # TODO: I suspect that this method could be simplified by using iterators
    sort_order = {}
    alexa_ranks = {}
    for line in open(config.ALEXA_PATH, 'r'):
        rank = line.strip().split(',')
        alexa_ranks[rank[1]] = rank[0]

    for canary in canaries:
        host = urlparse(canary.config["canary_url"]).hostname.split('.')
        host = "{0}.{1}".format(host[-2], host[-1])
        if host in alexa_ranks:
            sort_order[canary.slug] = int(alexa_ranks[host])
        else:
            # Randomly sort sites not in the top 1Mil
            sort_order[canary.slug] = randint(1000000, 2000000)

        # Move bad canary to the top
        if canary.get_state() == canary.STATE_BAD:
            sort_order[canary.slug] -= 2000000

    return sort_order

def text_report():
    """ Show status and diff for all canaries. Output as text """
    for path, dirnames, files in os.walk(CONFIG_DIR):
        if 'config.yml' in files:
            canary = Canary(path)
            print "Status for {0}: {1}".format(canary.config["name"], canary.get_state())
            if canary.get_state() != canary.STATE_OKAY:
                print canary.get_meta_value('last_error')

if __name__ == "__main__":
    _main()
