Canary Watch
========================


## Overview
Canarywatch is a set of scripts and a website to check Warrant Canaries to make sure they haven't unexpectedly changed or died. For an explanation of what a Warrant Canary is and a reference implementation of canarywatch please visit https://canarywatch.org. Canary Watch keeps a "recipie" for each canary it knows about in the config directory. Each recipie contains a config file, log file, logo image, and an empty module init file at minimum. The recipie may also contain a public PGP key used to sign the canary and any number of custom filter scripts.

When Canarywatch is run it first checks to see if the canary is due for another checkup (or has been forced to do a checkup). If so, the canary is fetched from the url specified in the config.yml file. The raw data is then put through the filters defined in config.yml, in order. Each filter takes in the output from the last filter and returns a new output, transforming and validating the text along the way (e.g. stripping dates, checking for expected changes, checking the pgp signature, etc.) Once the filters are done running the text is checked for unexpected diffs. The output of this whole process is stored under the datastore/<canary_name> directory, which is it's own git tree. 

The canary status is initially set to GOOD, if there are any errors or unexpected diffs within the checking process the status is changed to UNKOWN. If there are no errors then the status will remain the same. A Canarywatch operator may manually change the status fo a canary to BAD. This will cause the canary to appear differently on the generated site and not be checked for changes again. Status is only ever automatically changed to UNKOWN (though it is initialized as GOOD), which has no effect on the generated site. Only an operator may manually change status to GOOD or BAD for a given canary. If you are changing the status to BAD please note that you should also include an explanation of your change in site_log.yml for that canary, and link to a longer explanation; you probably want to talk to a lawyer before you do this. 

## Installing
1. Get the canary watch source code onto your server
2. Get the canary watch configs onto your server
3. Install the python packages listed in requirements.txt this can be done like so: `pip install`
4. Symlink the canarywatch-configs into the top level directory of canarywatch, call the symlink 'config'
5. Make a directory called datastore, git init the directory
6. symlink it into the top level canarywatch directory alongside config
7. Copy config_example.py to config.py and edit
8. Set up a cron job to check canaries, probably good to run once a day `cd /path/to/canarywatch/ && ./canarywatch check`
9. Set up a cron job to generate the site nightly `cd /path/to/canarywatch/ && ./canarywatch report`
10. Point your web server at /path/to/canarywatch/report/

## Contributing
Canary watch is written in Python and so any contributions should follow the PEP-8 style guide. Please clone the project on your own and send a git formatted patch to info@canarywatch.org or one of the maintainers directly. 
Questions can be sent to the same email address above. 

## Directory Structure

    -config
    |--$CANARY_1 # top level directory for any given canary
    |--$CANARY_n
    |----config.yml # receipie file for each canary
    |----pubkey.gpg (optional) # optional gpg key for checking canary sig.
    |----__init__.py # necessary to import the directory as a module
    |----logo.png #company logo for canary
    |----site_log.yml # file containing detailed info to put on the canarywatch site when generated
    |----*.py (optional) # various filters to run on the canary

    -datastore
    |--$CANARY_1
    |--$CANARY_n
    |----metadata #json file containing metadata about the last run
    |----canary_raw #raw downloaded canary data
    |----canary_clearn # sanitized canary data
    |----canary_diff #git diff of the last two canaries, if there is one
    |----status # file containing [ok, unsure, bad]
    |----logfile #log of every time the status changed. Format: -- date -- from -> to : reason

    -cwengine # Contains canary watch source files
    |--filters # Global filters that can be applied to any canary
    |--report # Directory where the generated report is placed
    |--templates # Jinja2 templates for generating the report
    |--config.py # Config file for canary watch

## Canary config parameters
Each canary "recipie" contains a file called config.yml which contains the basic steps necessary to check the canary. The possible paramters for that config file are as follows

* canary_url: url of canary
* canary_type: descripitve words about what the canary covers
* signed: boolean, whether the canary is signed with a gpg key
* period: how often the canary should be checked, in days
* name: human readable name of the org
* contact: string, who should be contacted when the canary goes down
* date_regex: A regular expression which matches the date to be filtered out of the canary
* filters: the list of filters (in order) that the canary should be run through

## Metadata file params
* url: url we got the canary from
* completion_time: time fetch was completed
* http_headers: http headers when request happened
* last_error: string containing last error message
* last_good: last date the canary was good
* archive_url: URL of a permanent archive of the canary



